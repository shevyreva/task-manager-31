package ru.t1.shevyreva.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.command.data.AbstractDataCommand;
import ru.t1.shevyreva.tm.command.data.DataBinaryLoadCommand;
import ru.t1.shevyreva.tm.command.data.DataBinarySaveCommand;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void start() {
        load();
        es.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void save() {
        bootstrap.processCommand(DataBinarySaveCommand.NAME, false);
    }

    public void load() {
        if (!Files.exists(Paths.get(AbstractDataCommand.FILE_BINARY))) return;
        bootstrap.processCommand(DataBinaryLoadCommand.NAME, false);
    }

    public void stop() {
        es.shutdown();
    }

}
