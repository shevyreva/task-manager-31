package ru.t1.shevyreva.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.request.ServerAboutRequest;
import ru.t1.shevyreva.tm.dto.request.ServerVersionRequest;
import ru.t1.shevyreva.tm.dto.response.ServerAboutResponse;
import ru.t1.shevyreva.tm.dto.response.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull ServerAboutResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull ServerVersionResponse getVersion(@NotNull ServerVersionRequest request);

}
